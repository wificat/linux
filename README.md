# Wifi C.A.T. 🐈
Installs a WPA2 Enterprise Network on GNU/Linux Systems.

## Compatibily
The Script is Desigend for NetworkManager and wpa_supplicant
### Tested OS Compatibily
The Script is tested __working__ with the following Distrubtions and Desktop Environments
  - [x] Ubuntu with Unity and Gnome (see #3)
  - [x] openSUSE Leap 42.3 with KDE and Xfce (see #5)
  - [x] Debian 9 with MATE (see #8)
  - [x] CentOS 7 with Gnome (see #10)
  - [x] Fedora 26 with Gnome (see #9)
  
=> The script should work with the Desktop Environments: Gnome, Unity, KDE, Xfce and MATE



The Script is __incompatible__ with
  - [ ] Debian 9 with LXDE -> wicd is not supported at all!

# Licence
> Wifi Configuration Assist Tool for Linux
> Copyright (C) 2017 Bennet Becker <bennet@becker-dd.de>
>
> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.
>
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program. If not, see <http://www.gnu.org/licenses/>.
